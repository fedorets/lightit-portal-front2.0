
import { Routes } from '@angular/router';

import { OwnProfileComponent } from './own-profile.component';

export const routes: Routes = [
    { path: '', component: OwnProfileComponent },
    { path: '**', component: OwnProfileComponent },
];
