
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { OwnProfileComponent } from './own-profile.component';
import { OwnProfileService } from './shared/own-profile.service';
import { routes } from './own-profile.routing';

@NgModule ({
    declarations: [
        OwnProfileComponent
    ],
    providers: [
        OwnProfileService
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule
    ]
})

export class OwnProfileModule {}

