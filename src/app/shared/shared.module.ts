import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { MaterializeModule } from 'ng2-materialize';

import { HeaderComponent } from './components/header/header.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { MobileMenuComponent } from './components/mobile-menu/mobile-menu.component';
import { TrustLetterLblComponent } from './components/trust-letter-lbl/trust-letter-lbl.component';
import { CanViewDirective } from './directives/canView.directive';
import { InputComponent } from './components/input/input.component';
import { ErrorsComponent } from './components/errors/errors.component';
import { UtilService } from './providers/util.service';
import { BirthdayWeekComponent } from './components/birthday-week/birthday-week.component';
import { BirthdayWeekService } from './providers/birtday-week.service';
import { ErrorService } from './providers/errors.service';
import { LazyImageComponent } from './components/lazy-image/lazy-image.components';
import { LoaderComponent } from './components/loader/loader.component';
import { DatepickerComponent } from './components/datepicker/datepicker.component';

@NgModule ({
    declarations: [
        HeaderComponent,
        NavigationComponent,
        MobileMenuComponent,
        InputComponent,
        TrustLetterLblComponent,
        ErrorsComponent,
        BirthdayWeekComponent,
        LazyImageComponent,
        CanViewDirective,
        LoaderComponent,
        DatepickerComponent
    ],
    exports: [
        HeaderComponent,
        NavigationComponent,
        MobileMenuComponent,
        TrustLetterLblComponent,
        InputComponent,
        ErrorsComponent,
        LazyImageComponent,
        CanViewDirective,
        BirthdayWeekComponent,
        LoaderComponent,
        DatepickerComponent,
        MaterializeModule
    ],
    imports: [
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        FormsModule,
        LazyLoadImageModule,
        MaterializeModule.forRoot()
    ],
    providers: [
        UtilService,
        BirthdayWeekService,
        ErrorService
    ]
})

export class SharedModule {}
