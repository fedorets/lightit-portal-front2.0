import { Directive, Output, EventEmitter, HostListener } from '@angular/core';

@Directive({
    selector: '[infinitiScroll]'
})

export class InfinitiScrollDirective {

    @Output() public endOfPage: EventEmitter<any> = new EventEmitter<any>();

    @HostListener('window:scroll')
    public scrollWindow() {
        let scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
        console.log(scrollTop);
        if (scrollTop + window.innerHeight + 300 >= window.document.body.scrollHeight) {
            this.endOfPage.emit();
        }
    }

}
