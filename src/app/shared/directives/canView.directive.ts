import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

import { SessionService } from '../../core/providers/session.service';

@Directive({ selector: '[canView]' })

export class CanViewDirective {

    constructor(private templateRef: TemplateRef<any>,
                private viewContainer: ViewContainerRef,
                private session: SessionService) {}

    @Input() set canView(groups: string[]) {
        let permissionGroups = this.session.permissions;
        let permitForView: boolean = false;
        for (let currGroup of groups) {
            for ( let group in permissionGroups) {
                if (permissionGroups.hasOwnProperty(group)) {
                    if (currGroup === group) {
                        permitForView = true;
                    }
                }
            }
        }

        if (permitForView) {
            this.viewContainer.createEmbeddedView(this.templateRef);
        } else {
            this.viewContainer.clear();
        }
    }
}

