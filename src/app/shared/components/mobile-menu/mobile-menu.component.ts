import { Component, EventEmitter, Output } from '@angular/core';

import { ConfigService } from '../../../core/providers/config.service';

@Component({
    selector: 'lt-mobile-menu',
    templateUrl: './mobile-menu.component.html',
    styleUrls: ['./mobile-menu.component.scss']
})

export class MobileMenuComponent {
    @Output() public close: EventEmitter<any>;
    public imgPath: string;

    constructor() {
        this.close = new EventEmitter();
        this.imgPath = ConfigService.imgPath;
    }

    public closeSideNav(event) {
        event.preventDefault();
        this.close.emit();
    }
}
