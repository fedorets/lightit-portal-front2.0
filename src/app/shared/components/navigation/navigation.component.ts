import { Component, EventEmitter, Output } from '@angular/core';

import { ConfigService } from '../../../core/providers/config.service';

@Component({
    selector: 'lt-nav',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.scss']
})

export class NavigationComponent {
    public imgPath: string;
    @Output() public logout: EventEmitter<any>;

    constructor() {
        this.imgPath = ConfigService.imgPath;
        this.logout = new EventEmitter();
    }

    public logOut() {
       this.logout.emit();
    }
}
