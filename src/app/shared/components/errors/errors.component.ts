import { Component, Input } from '@angular/core';

@Component({
  selector: 'lt-error',
  templateUrl: './errors.component.html',
  styleUrls: ['./errors.component.scss']
})

export class ErrorsComponent {
  @Input() public control;
}
