import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import moment from 'moment';

@Component({
    selector: 'lt-datepicker',
    templateUrl: './datepicker.component.html',
    styleUrls: ['./datepicker.component.scss']
})

export class DatepickerComponent implements OnInit {
    @Input() public min: any = true;
    @Input() public max: string;
    @Input() public label: string;
    @Input() public control: FormControl;
    @Input() public format: string = 'dd-mm-yyyy';
    @Input() public formatSubmit: string = 'mm-dd-yyyy';
    @Input() public closeBtnText: string = '';
    @Input() public clearBtnText: string = '';
    @Input() public todayBtnText: string = '';
    public id: string;
    public datepickerOption: any;

    constructor() {
        this.id = 'datepicker' + (+new Date() * Math.random());
    }

    public ngOnInit() {
        this.datepickerOption = {
            format: this.format,
            formatSubmit: this.formatSubmit,
            close: this.closeBtnText,
            monthsFull: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль',
            'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthsShort: ['Янв', 'Февр', 'Март', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг',
            'Сент', 'Окт', 'Нояб', 'Дек'],
            weekdaysFull: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            weekdaysShort: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда',
            'Четверг', 'Пятница', 'Субота'],
            showMonthsShort: false,
            showWeekdaysFull: true,
            today: this.todayBtnText,
            clear: this.clearBtnText,
            min: this.min,
            max: this.prepareMaxDate()
        };
    }

    private prepareMaxDate(str = false) {
        if (str) {
            return [
                moment(str).add(1, 'year').format('YYYY'),
                moment(str).subtract(1, 'month').format('MM'),
                moment(str).format('DD')
            ];
        } else {
           return [
                moment().add(1, 'year').format('YYYY'),
                moment().subtract(1, 'month').format('MM'),
                moment().format('DD')
            ];
        }
    }
}
