import { Component, Input, ViewEncapsulation } from '@angular/core';

import { ConfigService } from '../../../core/providers/config.service';

@Component({
  selector: 'lt-lazy-img',
  templateUrl: './lazy-image.components.html',
  encapsulation: ViewEncapsulation.None
})

export class LazyImageComponent {
  @Input() public styleClass: string = null;
  @Input() public image: string;
  @Input() public defaultImage: string = ConfigService.defaultImageUser;
  @Input() public alt: string = 'photo';
}
