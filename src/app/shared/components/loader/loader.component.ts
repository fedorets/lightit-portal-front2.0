import { Component } from '@angular/core';

@Component({
    selector: 'lt-loader',
    templateUrl: './loader.component.html',
    styleUrls: ['./loader.component.scss']
})

export class LoaderComponent {}
