import { Component, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Router } from '@angular/router';
import { MzSidenavComponent} from 'ng2-materialize';

import { ConfigService } from '../../../core/providers/config.service';
import { AuthService } from '../../../core/providers/auth.service';

@Component({
    selector: 'lt-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnDestroy {
    public imgPath: string;
    public isAuthorized: boolean;
    public authSubscription: Subscription;

    constructor(private authService: AuthService,
                private router: Router) {
        this.imgPath = ConfigService.imgPath;
        this.authSubscription = this.authService.isListenAuthorization().subscribe((res) => {
            this.isAuthorized = res;
        });
    }

    public logout() {
        this.authService.logout().subscribe((res) => {
            console.log('logout!');
            this.router.navigate(['/login']);
        });
    }

    public ngOnDestroy() {
        this.authSubscription.unsubscribe();
    }
}
