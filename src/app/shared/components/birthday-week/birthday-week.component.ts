import { Component } from '@angular/core';

import { BirthdayWeekService } from '../../providers/birtday-week.service';
import { UserBirthday } from '../../../core/models/user-birthday.model';

@Component({
    selector: 'lt-birthday-week',
    styleUrls: ['./birthday-week.component.scss'],
    templateUrl: './birthday-week.component.html'
})

export class BirthdayWeekComponent {
    public birthdayWeekList: UserBirthday[];
    public birthdayTodayList: UserBirthday[];

    constructor(private birthdayWeekService: BirthdayWeekService) {
        this.birthdayWeekService.getBirthdayWeek().subscribe((res) => {
            this.birthdayWeekList = res.week;
            this.birthdayTodayList = res.today;
            console.log(this.birthdayTodayList, this.birthdayWeekList);
        }, (err) => {
            console.log('Ошибка получения ДР недели.', err);
        });
    }
}
