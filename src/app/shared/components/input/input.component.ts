
import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
    selector: 'lt-input',
    templateUrl: './input.component.html',
    styleUrls: ['./input.component.scss']
})

export class InputComponent {
    @Input() public type = 'text';
    @Input() public label = null;
    @Input() public length: false;
    @Input() public control: FormControl = null;
    @Input() public isTextarea: boolean = false;
    @Input() public icon: string;
    public id: string;

    constructor() {
        this.id = 'input' + (+new Date() * Math.random());
    }
}
