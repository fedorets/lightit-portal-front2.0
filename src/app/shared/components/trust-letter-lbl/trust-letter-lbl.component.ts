import { Component } from '@angular/core';

import { ConfigService } from '../../../core/providers/config.service';

@Component({
    selector: 'lt-trust-letter-lbl',
    templateUrl: './trust-letter-lbl.component.html',
    styleUrls: ['./trust-letter-lbl.component.scss']
})

export class TrustLetterLblComponent {
    public imgPath: string;

    constructor() {
        this.imgPath = ConfigService.imgPath;
    }
}
