import { AbstractControl, Validators } from '@angular/forms';

import { RegExpService } from './reg-exp.service';

export class LtValidators extends Validators {
    public static passwordMatch(control: AbstractControl) {
        let password = control.get('new_password1').value;
        let confirm = control.get('new_password2').value;
        if (confirm && password) {
            if (password !== confirm) {
                control.get('new_password2').setErrors({passwordMatch: true});
            } else {
                return null;
            }
        }
    }

    public static checkEmail(control: AbstractControl) {
        if (control.value !== null) {
            if (control.value.length > 0) {
                return !RegExpService.email.test(control.value) ? {patternEmail: true} : null;
            }
        }
    }

    constructor() {
        super();
    }
}
