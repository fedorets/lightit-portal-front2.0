import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ConfigService } from '../../core/providers/config.service';
import { ErrorService } from './errors.service';
import { UserBirthday } from '../../core/models/user-birthday.model';

@Injectable()
export class BirthdayWeekService {
    constructor(private http: HttpClient,
                private errorService: ErrorService) {}

    /**
     * @returns array of users, which have birthday on this week or last weekends
     */
    public getBirthdayWeek() {
        return this.http.get(ConfigService.birthWeekPath).map((res) => {
            return this.orderBirthdayDays(res);
        }).catch((err) => this.errorService.getErrors(err));
    }

    private orderBirthdayDays(data) {
        let birthdayToday = [];
        let birthdayWeek = [];
        data.forEach((i) => {
            let user = new UserBirthday(i);
            (user.isBirthToday) ? birthdayToday.push(user) : birthdayWeek.push(user);
        });
        return {
            week: birthdayWeek,
            today: birthdayToday
        };
    }
}
