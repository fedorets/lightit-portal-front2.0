import { Injectable } from '@angular/core';
import { MzToastService } from 'ng2-materialize';

@Injectable()
export class UtilService {
    constructor(private toastService: MzToastService) {}
    public getMaterializeToast (text, state = 'success', duration = 2000) {
        switch (state) {
            case 'success':
                this.toastService.show(text, duration, 'green');
                break;
            case 'inform':
                this.toastService.show(text, duration, 'orange');
                break;
            case 'failure':
                this.toastService.show(text, duration, 'red');
                break;
            default: break;
        }
    }
}
