import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';

@Injectable()
export class ErrorService {
    public errorMessages: any = {
            required: 'Обязательное поле.',
            pattern: 'Шаблон не валидный.',
            minlength: 'Нужно больше символов.',
            maxlength: 'Нужно меньше символов.',
            patternEmail: 'Неверный формат email.',
            passwordMatch: 'Пароли не совпадают.',
            email: 'Неверный формат email.'
    };

    public getErrors(response) {
        let messages = [];
        for (let err in response.error) {
            if (response.error.hasOwnProperty(err)) {
                messages.push(response.error[err]);
            }
        }
        return Observable.throw(messages);
    }
}
