import { Component } from '@angular/core';

@Component({
    selector: 'lt-admin-vacations-panel',
    templateUrl: './admin-vacations-panel.component.html',
    styleUrls: ['admin-vacations-panel.component.scss']
})

export class AdminVacationsPanelComponent {}