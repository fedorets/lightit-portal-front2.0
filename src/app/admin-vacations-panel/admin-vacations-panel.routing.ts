
import { Routes } from '@angular/router';

import { AdminVacationsPanelComponent } from './admin-vacations-panel.component';

export const routes: Routes = [
    { path: '', component:  AdminVacationsPanelComponent },
    { path: '**', component: AdminVacationsPanelComponent  },
];
