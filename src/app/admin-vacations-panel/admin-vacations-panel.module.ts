import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { AdminVacationsPanelComponent } from './admin-vacations-panel.component';
import { routes } from './admin-vacations-panel.routing';

@NgModule ({
    declarations: [
        AdminVacationsPanelComponent
    ],
    providers: [],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule
    ]
})

export class AdminVacationsPanelModule {}
