import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../shared/shared.module';
import { routes } from './personal-plan.routing';

import { AllPersonalPlansComponent } from './all-personal-plans/all-personal-plans.component';
import { AvailablePersonalPlansComponent } from './available-personal-plans/available-personal-plans.component';
import { PersonalPlanComponent } from './own-personal-plan/own-personal-plan.component';

@NgModule({
    declarations: [
        AllPersonalPlansComponent,
        AvailablePersonalPlansComponent,
        PersonalPlanComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule
    ]
})

export class PersonalPlanModule {}
