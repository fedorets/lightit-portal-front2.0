import { Component } from '@angular/core';

@Component({
    selector: 'lt-all-personal-plans',
    templateUrl: './all-personal-plans.component.html',
    styleUrls: ['all-personal-plans.component.scss']
})

export class AllPersonalPlansComponent {}