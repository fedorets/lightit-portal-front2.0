import { Component } from '@angular/core';

@Component({
    selector: 'lt-available-personal-plans',
    templateUrl: './available-personal-plans.component.html',
    styleUrls: ['./available-personal-plans.component.scss']
})

export class AvailablePersonalPlansComponent {}
