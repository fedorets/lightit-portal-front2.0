import { Routes } from '@angular/router';

import { PersonalPlanComponent } from './own-personal-plan/own-personal-plan.component';
import { AllPersonalPlansComponent } from './all-personal-plans/all-personal-plans.component';
import { AvailablePersonalPlansComponent } from './available-personal-plans/available-personal-plans.component';
import { AuthGuard } from '../core/guards/auth.guard';
import { EnvGuard } from '../core/guards/env.guard';
import { PermissionGuard } from '../core/guards/permissions.guard';

export const routes: Routes = [
    { path: '', component: PersonalPlanComponent },
    {
        path: 'own',
        component: PersonalPlanComponent,
        canActivate: [AuthGuard, EnvGuard],
        data: {
            namePage: 'personalPlan',
        }
    }, {
        path: 'all-list',
        component: AllPersonalPlansComponent,
        canActivate: [AuthGuard, EnvGuard]
    }, {
        path: 'available',
        component: AvailablePersonalPlansComponent,
        canActivate: [AuthGuard, EnvGuard]
    }
];
