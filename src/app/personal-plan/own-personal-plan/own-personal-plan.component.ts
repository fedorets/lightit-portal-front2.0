import { Component } from '@angular/core';

@Component({
    selector: 'lt-own-personal-plan',
    templateUrl: './own-personal-plan.component.html',
    styleUrls: ['./own-personal-plan.component.scss']
})

export class PersonalPlanComponent {}
