import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { BirthdayComponent } from './birthday.component';
import { BirthdayService } from './shared/birthday.service';
import { routes } from './birthday.routing';

@NgModule ({
    declarations: [
        BirthdayComponent
    ],
    providers: [
        BirthdayService
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule
    ]
})

export class BirthdayModule {}
