import { Routes } from '@angular/router';

import { BirthdayComponent } from './birthday.component';

export const routes: Routes = [
    { path: '', component: BirthdayComponent, pathMatch: 'full' },
    { path: '**', component: BirthdayComponent }
];
