import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { AuthService } from '../../core/providers/auth.service';
import { UtilService } from '../../shared/providers/util.service';
import { LtValidators } from '../../shared/providers/validators.service';
import { ErrorService } from '../../shared/providers/errors.service';

@Component({
    selector: 'lt-password-reset',
    templateUrl: './password-reset.component.html',
    styleUrls: ['./password-reset.component.scss']
})

export class PasswordResetComponent implements OnInit {
    public resetPasswordForm: FormGroup;
    public errorMessages: any;

    constructor(private authService: AuthService,
                private fb: FormBuilder,
                private route: ActivatedRoute,
                private utilService: UtilService,
                private router: Router,
                private errorService: ErrorService) {
        this.errorMessages = this.errorService.errorMessages;

        this.resetPasswordForm = fb.group({
            new_password1: [null, [
                Validators.required,
                Validators.minLength(8),
            ]],
            new_password2: [null, [
                Validators.required,
                Validators.minLength(8)
            ]],
            uid: [null],
            token: [null]
        }, { validator: LtValidators.passwordMatch });
    }

    public submit(event, form) {
        event.preventDefault();
        this.authService.confirmResetPassword(form.value).subscribe((res) => {
            this.utilService.getMaterializeToast('Пароль был успешно заменен.');
            this.router.navigate(['/login']);
        }, (err) => this.utilService.getMaterializeToast(err[0], 'failure'));
    }

    public ngOnInit() {
        this.route.params.subscribe((res) => {
            this.resetPasswordForm.patchValue({uid: res.uid, token: res.token});
        });
    }
}
