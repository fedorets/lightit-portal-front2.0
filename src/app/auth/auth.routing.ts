import { Routes } from '@angular/router';

import { StartPageComponent } from './start-page/start-page.component';
import { PasswordResetComponent } from './password-reset/password-reset.component';

export const authRoutes: Routes = [
    { path: '', component: StartPageComponent, pathMatch: 'full' },
    { path: 'password_reset/:uid/:token', component: PasswordResetComponent },
    { path: '**', component: StartPageComponent },
];
