import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MzBaseModal } from 'ng2-materialize';

import { LtValidators } from '../../shared/providers/validators.service';
import { AuthService } from '../../core/providers/auth.service';
import { UtilService } from '../../shared/providers/util.service';

@Component({
    selector: 'lt-password-reset-modal',
    templateUrl: './password-reset-modal.component.html',
    styleUrls: ['./password-reset-modal.component.scss']
})

export class PasswordResetModalComponent extends MzBaseModal {
    public resetPasswordForm: FormGroup;
    @Input() public errorMessages: any;

    constructor(private authService: AuthService,
                private utilService: UtilService) {
        super();
        this.resetPasswordForm = new FormGroup({
            email: new FormControl(null, [
                Validators.required,
                Validators.minLength(7),
                LtValidators.checkEmail
            ])
        });
    }
    public change(v) {
        console.log(v);
    }

    public submit(event, form) {
        event.preventDefault();
        this.authService.resetPassword(form.value).subscribe(() => {
            this.utilService.getMaterializeToast(`На ${form.value.email} выслана ссылка на восстановление пароля.`);
        }, (err) => {
            this.utilService.getMaterializeToast(err[0], 'failure');
            this.resetPasswordForm.reset();
        });
    }
}
