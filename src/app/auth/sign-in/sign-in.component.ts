import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MzModalService } from 'ng2-materialize';

import { AuthService } from '../../core/providers/auth.service';
import { LtValidators } from '../../shared/providers/validators.service';
import { UtilService } from '../../shared/providers/util.service';
import { PasswordResetModalComponent } from '../password-reset-modal/password-reset-modal.component';
import { ErrorService } from '../../shared/providers/errors.service';

@Component({
    selector: 'lt-sign-in',
    templateUrl: './sign-in.component.html',
    styleUrls: ['./sign-in.component.scss']
})

export class SignInComponent {
    public signInForm: FormGroup;
    public errorMessages: any;

    constructor(private fb: FormBuilder,
                private authService: AuthService,
                private utilService: UtilService,
                private router: Router,
                private modalService: MzModalService,
                private errorService: ErrorService) {
        this.signInForm = fb.group({
            email: [null, [
                Validators.required,
                Validators.minLength(7),
                LtValidators.checkEmail]],
            password: [null, [
                Validators.required,
                Validators.minLength(8)]]
        });

        this.errorMessages = this.errorService.errorMessages;
    }

    public submit(e, form) {
        e.preventDefault();
        if (form.valid) {
            this.authService.signIn(form.value).subscribe(() => {
                this.router.navigate(['']);
            }, (err) => this.utilService.getMaterializeToast(err[0], 'failure'));
        } else {
            this.utilService.getMaterializeToast('Ошибка заполнения формы', 'failure');
        }
    }

    public change(val) {
        console.log(val);
    }

    public openModal() {
        this.modalService.open(PasswordResetModalComponent, {errorMessages: this.errorMessages});
    }
}
