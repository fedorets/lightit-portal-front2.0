import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { StartPageComponent } from './start-page/start-page.component';
import { SharedModule } from '../shared/shared.module';
import { authRoutes } from './auth.routing';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { PasswordResetModalComponent } from './password-reset-modal/password-reset-modal.component';
import { PasswordResetComponent } from './password-reset/password-reset.component';

@NgModule({
    declarations: [
        StartPageComponent,
        SignInComponent,
        SignUpComponent,
        PasswordResetModalComponent,
        PasswordResetComponent
    ],
    imports: [
        SharedModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(authRoutes)
    ],
    entryComponents: [
        PasswordResetModalComponent
    ]
})

export class AuthModule {}
