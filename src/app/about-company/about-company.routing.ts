import { Routes } from '@angular/router';

import { MissionComponent } from './mission/mission.component';
import { InformComponent } from './inform/inform.component';
import { DevCodexComponent } from './dev-codex/dev-codex.component';
import { RulesComponent } from './rules/rules.component';

export const aboutCompanyRoutes: Routes = [
    { path: '', component: MissionComponent, pathMatch: 'full' },
    { path: 'mission',  component: MissionComponent },
    { path: 'inform',  component: InformComponent },
    { path: 'dev-codex',  component: DevCodexComponent },
    { path: 'rules',  component: RulesComponent },
    { path: '**', component: MissionComponent },
];
