import { Component } from '@angular/core';

import { ConfigService } from '../../core/providers/config.service';

@Component({
    selector: 'lt-rules',
    templateUrl: './rules.component.html',
    styleUrls: ['./rules.component.scss']
})

export class RulesComponent {
    public imgPath: string;

    constructor() {
        this.imgPath = ConfigService.imgPath;
    }
}
