import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DevCodexComponent } from './dev-codex/dev-codex.component';
import { InformComponent } from './inform/inform.component';
import { MissionComponent } from './mission/mission.component';
import { RulesComponent } from './rules/rules.component';
import { SharedModule } from '../shared/shared.module';
import { aboutCompanyRoutes } from './about-company.routing';

@NgModule({
    declarations: [
        RulesComponent,
        MissionComponent,
        InformComponent,
        DevCodexComponent,
    ],
    imports: [
        SharedModule,
        RouterModule.forChild(aboutCompanyRoutes)
    ]
})

export class AboutCompanyModule {}
