import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { EmployeesComponent } from './employees.component';
import { EmployeesService } from './shared/employees.service';
import { routes } from './employees.routing';

@NgModule ({
    declarations: [
        EmployeesComponent
    ],
    providers: [
        EmployeesService
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule
    ]
})

export class EmployeesModule {}
