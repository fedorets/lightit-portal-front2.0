import { Routes } from '@angular/router';

import { EmployeesComponent } from './employees.component';

export const routes: Routes = [
    { path: '', component: EmployeesComponent, pathMatch: 'full' },
    { path: '**', component: EmployeesComponent }
];
