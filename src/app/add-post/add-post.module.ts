import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { AddPostBtnComponent } from './add-post-btn/add-post-btn.component';
import { AddPostModalComponent } from './add-post-modal/add-post-modal.component';
import { AdvertFormComponent } from './advert-form/advert-form.component';
import { EventFormComponent } from './event-form/event-form.component';
import { PollFormComponent } from './poll-form/poll-form.component';
import { AddPostService } from './shared/add-post.service';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        AddPostBtnComponent,
        AddPostModalComponent,
        AdvertFormComponent,
        EventFormComponent,
        PollFormComponent
    ],
    entryComponents: [
        AddPostModalComponent
    ],
    exports: [
        AddPostBtnComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        ReactiveFormsModule
    ],
    providers: [
        AddPostService
    ]
})

export class AddPostModule {}
