import { Component } from '@angular/core';
import { MzModalService } from 'ng2-materialize';

import { AddPostModalComponent } from '../add-post-modal/add-post-modal.component';

@Component({
    selector: 'lt-add-post-btn',
    templateUrl: './add-post-btn.component.html',
    styleUrls: ['./add-post-btn.component.scss']
})

export class AddPostBtnComponent {
    constructor(private modalService: MzModalService) {}

    public showModal() {
        this.modalService.open(AddPostModalComponent);
    }
}
