import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ConfigService } from '../../core/providers/config.service';

@Injectable()
export class AddPostService {
    constructor(private http: HttpClient) {}

    public createNews(data) {
        return this.http.post(ConfigService.messagesPath, data);
    }
}
