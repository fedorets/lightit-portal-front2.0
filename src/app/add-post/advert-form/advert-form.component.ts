import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { UtilService } from '../../shared/providers/util.service';
import { AddPostService } from '../shared/add-post.service';
import { ErrorService } from '../../shared/providers/errors.service';

import moment from 'moment';

@Component({
    selector: 'lt-advert-form',
    templateUrl: './advert-form.component.html',
    styleUrls: ['./advert-form.component.scss']
})

export class AdvertFormComponent {
    public advertForm: FormGroup;
    public errorMessages: any;
    constructor(private fb: FormBuilder,
                private utilService: UtilService,
                private addPostService: AddPostService,
                private errorService: ErrorService) {
        this.advertForm = this.fb.group({
            message_type: ['advert'],
            text: [null, [Validators.maxLength(3000),
                          Validators.required]],
            title: [null, [Validators.required,
                Validators.minLength(2),
                Validators.maxLength(60)]],
            is_important: [false]
        });

        this.advertForm.get('is_important').valueChanges.subscribe((state) => {
            (state) ? this.advertForm.addControl('relevance_dt', new FormControl(null, [Validators.required])) : this.advertForm.removeControl('relevance_dt');
        });

        this.errorMessages = this.errorService.errorMessages;
    }

    public submit(e, form) {
        e.preventDefault();
        console.log(form.value);
        if (form.valid) {
            this.changeRelevanceDate();
            this.addPostService.createNews(form.value).subscribe((res) => {
                console.log(res);
            }, () => this.utilService.getMaterializeToast('Ошибка создания новости', 'failure'));
        } else {
            this.utilService.getMaterializeToast('Проверьте, введенную вами информацию.');
        }
    }

    private changeRelevanceDate() {
        if (this.advertForm.contains('relevance_dt')) {
            this.advertForm.patchValue({
                relevance_dt: moment(this.advertForm.get('relevance_dt').value).format()
            });
        }
    }
}
