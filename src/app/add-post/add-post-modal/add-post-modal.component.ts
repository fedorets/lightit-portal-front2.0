import { Component } from '@angular/core';
import { MzBaseModal } from 'ng2-materialize';

@Component({
    selector: 'lt-add-post-modal',
    templateUrl: './add-post-modal.component.html',
    styleUrls: ['./add-post-modal.component.scss']
})

export class AddPostModalComponent extends MzBaseModal {
    public activeType: string;
    constructor() {
        super();
        this.activeType = 'advert';
    }

    public changeActiveType(type) {
        this.activeType = type;
    }

}
