
import { Routes } from '@angular/router';

import { AdminUsersPanelComponent } from './admin-users-panel.component';

export const routes: Routes = [
    { path: '', component: AdminUsersPanelComponent },
    { path: '**', component: AdminUsersPanelComponent },
];
