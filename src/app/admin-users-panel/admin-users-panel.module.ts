import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { AdminUsersPanelComponent } from './admin-users-panel.component';
import { routes } from './admin-users-panel.routing';

@NgModule ({
    declarations: [
        AdminUsersPanelComponent
    ],
    providers: [],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule
    ]
})

export class AdminUsersPanelModule {}
