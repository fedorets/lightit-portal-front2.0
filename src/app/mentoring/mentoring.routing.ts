import { Routes } from '@angular/router';

import { MentoringComponent } from './mentoring.component';

export const routes: Routes = [
    { path: '', component: MentoringComponent, pathMatch: 'full' },
    { path: '**', component: MentoringComponent }
];
