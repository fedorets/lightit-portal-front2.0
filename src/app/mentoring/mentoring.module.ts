import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { MentoringService } from './shared/mentoring.service';
import { MentoringComponent } from './mentoring.component';
import { routes } from './mentoring.routing';

@NgModule ({
    declarations: [
        MentoringComponent
    ],
    providers: [
        MentoringService
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule
    ]
})

export class MentoringModule {}
