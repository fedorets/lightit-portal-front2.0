import { Routes } from '@angular/router';

import { NoContentComponent } from './no-content';

import { DataResolver } from './app.resolver';
import { AuthGuard } from './core/guards/auth.guard';
import { EnvGuard } from './core/guards/env.guard';
import { PermissionGuard } from './core/guards/permissions.guard';
import { PermissionService } from './core/providers/permission.service';

export const ROUTES: Routes = [{
    path: '',
    loadChildren: './news/news.module#NewsModule',
    pathMatch: 'full',
    canActivate: [AuthGuard, EnvGuard],
    data: {
      namePage: 'news'
    }
  }, {
    path: 'login',  loadChildren: './auth/auth.module#AuthModule',
    canActivate: [AuthGuard]
  }, {
    path: 'news',
    loadChildren: './news/news.module#NewsModule',
    canActivate: [AuthGuard, EnvGuard],
    data: {
      namePage: 'news'
    }
  }, {
    path: 'birthday',
    loadChildren: './birthday/birthday.module#BirthdayModule',
    canActivate: [AuthGuard, EnvGuard],
    data: {
        namePage: 'birthday'
    }
  }, {
    path: 'employees',
    loadChildren: './employees/employees.module#EmployeesModule',
    canActivate: [AuthGuard, EnvGuard],
    data: {
        namePage: 'employees'
    }
  }, {
    path: 'about-company',
    loadChildren: './about-company/about-company.module#AboutCompanyModule',
    canActivate: [AuthGuard, EnvGuard],
    data: {
        namePage: 'mission',
    }
  }, {
    path: 'mentoring',
    loadChildren: './mentoring/mentoring.module#MentoringModule',
    canActivate: [AuthGuard, EnvGuard],
    data: {
        namePage: 'mentoring',
    }
  }, {
    path: 'gallery',
    loadChildren: './gallery/gallery.module#GalleryModule',
    canActivate: [AuthGuard, EnvGuard],
    data: {
        namePage: 'gallery',
    }
  }, {
    path: 'edit-profile',
    loadChildren: './own-profile/own-profile.module#OwnProfileModule',
    canActivate: [AuthGuard, EnvGuard],
    data: {
        namePage: 'editProfile',
    }
  }, {
    path: 'vacations',
    loadChildren: './vacations/vacations.module#VacationsModule',
    canActivate: [AuthGuard, EnvGuard],
    data: {
        namePage: 'vacations',
    }
  }, {
    path: 'personal-plan',
    loadChildren: './personal-plan/personal-plan.module#PersonalPlanModule',
    canActivate: [AuthGuard, EnvGuard],
    data: {
        namePage: 'personalPlan',
    }
  }, {
    path: 'admin-users-panel',
    loadChildren: './admin-users-panel/admin-users-panel.module#AdminUsersPanelModule',
    canActivate: [AuthGuard, EnvGuard],
    data: {
        namePage: 'adminUsersPanel'
    }
  }, {
    path: 'admin-vacations-panel',
    loadChildren: './admin-vacations-panel/admin-vacations-panel.module#AdminVacationsPanelModule',
    canActivate: [AuthGuard, EnvGuard, PermissionGuard],
    data: {
        permissions: [PermissionService.PERMISSION_GROUPS.VACATION_REVIEWERS],
        namePage: 'adminVacationsPanel'
    }
  },
  { path: '**',    component: NoContentComponent },
];
