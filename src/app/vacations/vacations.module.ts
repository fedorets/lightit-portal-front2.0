import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { VacationsService } from './shared/vacations.service';
import { VacationsComponent } from './vacations.component';
import { routes } from './vacations.routing';

@NgModule ({
    declarations: [
        VacationsComponent
    ],
    providers: [
        VacationsService
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule
    ]
})

export class VacationsModule {}
