import { Component } from '@angular/core';

@Component({
    selector: 'lt-vacations',
    templateUrl: './vacations.component.html',
    styleUrls: ['vacations.component.scss']
})

export class VacationsComponent {}
