
import { Routes } from '@angular/router';

import { VacationsComponent } from './vacations.component';

export const routes: Routes = [
    { path: '', component: VacationsComponent },
    { path: '**', component: VacationsComponent },
];
