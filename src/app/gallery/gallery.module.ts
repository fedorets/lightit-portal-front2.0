import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { GalleryComponent } from './gallery.component';
import { GalleryService } from './shared/gallery.service';
import { routes } from './gallery.routing';

@NgModule ({
    declarations: [
        GalleryComponent
    ],
    providers: [
        GalleryService
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule
    ]
})

export class GalleryModule {}
