import { Routes } from '@angular/router';

import { GalleryComponent } from './gallery.component';

export const routes: Routes = [
    { path: '', component: GalleryComponent },
    { path: '**', component: GalleryComponent },
];
