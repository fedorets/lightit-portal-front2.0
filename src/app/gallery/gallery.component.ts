import { Component } from '@angular/core';

@Component({
    selector: 'lt-gallery',
    templateUrl: './gallery.component.html',
    styleUrls: ['gallery.component.scss']
})

export class GalleryComponent {}
