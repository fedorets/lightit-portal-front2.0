import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import {  ActivatedRouteSnapshot,
    CanActivate,
    Router,
    RouterStateSnapshot
} from '@angular/router';

import { SessionService } from '../providers/session.service';

@Injectable()
export class PermissionGuard implements CanActivate {
    constructor(private router: Router,
                private session: SessionService) {
    }

    /**
     * Used for protection pages
     * @param next - current route
     * @returns permission
     */
    public canActivate(next: ActivatedRouteSnapshot,
                       state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        let permissions = next.data.permissions;
        let currentsPermissions = this.session.permissions;
        let permitForView: boolean = false;
        for (let availablePermission of permissions) {
            for ( let currPerm in currentsPermissions) {
                if (currPerm === availablePermission) {
                    permitForView = true;
                    console.log('I have permissions.');
                }
            }
        }
        if (permitForView) {
            return true;
        } else {
            console.log('I do not have permissions.');
            this.router.navigate(['']);
            return false;
        }
    }
}
