import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import {  ActivatedRouteSnapshot,
    CanActivate,
    RouterStateSnapshot
} from '@angular/router';

import { EnvironmentService } from '../providers/environment.service';

@Injectable()
export class EnvGuard implements CanActivate {
    constructor(private environmentService: EnvironmentService) {
    }

    public canActivate(next: ActivatedRouteSnapshot,
                       state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        let namePage = next.data.namePage;
        let path = this.environmentService.isProductionLink(namePage);
        if (path) {
            window.location.href = path;
            return false;
        } else {
            console.log('does not exist link or not production env');
            return true;
        }
    }
}
