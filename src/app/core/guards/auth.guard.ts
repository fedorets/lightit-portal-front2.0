import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import {  ActivatedRouteSnapshot,
    CanActivate,
    Router,
    RouterStateSnapshot
} from '@angular/router';

import { AuthService } from '../providers/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private authService: AuthService,
                private router: Router) {
    }

    /**
     * Used for protection pages
     * @param next - current route
     * @returns permission
     */
    public canActivate(next: ActivatedRouteSnapshot,
                       state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        let isLoginPage = state.url.indexOf('login') !== -1;
        if (!this.authService.isLogin() && !isLoginPage) {
            this.router.navigate(['/login']);
            return false;
        } else if (this.authService.isLogin() && isLoginPage) {
            this.router.navigate(['']);
            return false;
        } else {
            return true;
        }
    }
}
