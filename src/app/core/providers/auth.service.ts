import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { SessionService } from './session.service';
import { ConfigService } from './config.service';
import { ErrorService } from '../../shared/providers/errors.service';

@Injectable()
export class AuthService {
    private loginSbj = new BehaviorSubject(this.isLogin());
    constructor(private session: SessionService,
                private http: HttpClient,
                private errorService: ErrorService) {}

    /**
     * check does user exist
     * @returns user
     */
    public isLogin() {
        return !!(this.session.token);
    }

    /**
     * For login authorization and saving token && user in the session
     * @param data - object consist of email and password
     * @returns object of User and token
     */
    public signIn(data): any {
        return this.http.post(ConfigService.loginPath, data).map((res: any) => {
            this.session.user = res;
            this.session.permissions = res.user.permission_groups;
            this.loginSbj.next(true);
            return res;
        }).catch((err) => this.errorService.getErrors(err));
    }

    /**
     * for subscription, is user autorized
     */
    public isListenAuthorization(): Observable<any> {
        return this.loginSbj.asObservable();
    }

    /**
     * For remove data user from session (token and localStorage)
     */
    public logout() {
        return this.http.post(ConfigService.logoutPath, '').map((res) => {
            this.session.user = null;
            this.session.permissions = null;
            this.loginSbj.next(false);
            return res;
        });
    }

    /**
     * for reset password in signIn tab
     * @param email - object looks like { "email": "someEmail@gmail.com" }
     * @returns success or failure
     */
    public resetPassword(email) {
        return this.http.post(ConfigService.resetPasswordPath, email)
            .map((res) => res)
            .catch((err) => this.errorService.getErrors(err));
    }

    /**
     * after email confirm
     * @param data - two passwords, token and uid
     * @returns success or failure
     */
    public confirmResetPassword(data) {
       return this.http.post(ConfigService.confirmResetPasswordPath, data)
           .map((res) => res)
           .catch((err) => this.errorService.getErrors(err));
    }
}
