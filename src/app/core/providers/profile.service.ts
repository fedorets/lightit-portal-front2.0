import { Injectable } from '@angular/core';

import { SessionService } from './session.service';
import { User } from '../models/user.model';

@Injectable()
export class ProfileService {
    constructor(private session: SessionService) {}
    public get user() {
        return new User(this.session.user);
    }
}
