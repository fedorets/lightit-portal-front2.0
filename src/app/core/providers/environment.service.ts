import { Injectable } from '@angular/core';

import { ConfigService } from './config.service';

@Injectable()
export class EnvironmentService {
    private originalLinks: any;

    constructor() {
        this.originalLinks = {
            mission: 'mission',
            news: 'news',
            birthday: 'calendar',
            employees: 'staff',
            mentoring: 'mentorship',
            gallery: 'life',
            editProfile: 'edit-user',
            vacations: 'vacation',
            personalPlan: 'plans',
            adminUsersPanel: 'admin',
            adminVacationsPanel: 'vacation-admin'
        };
    }

    public isProductionLink(pageName) {
        if (process.env.ISPRODUCTION) {
            let link = this.getProductionLink(pageName);
            return link ? `${ConfigService.basePath}#/pages/${link}` : false;
        } else {
            return false;
        }
    }

    public getProductionLink(pageName) {
        for (let linkName in this.originalLinks) {
            if (this.originalLinks.hasOwnProperty(linkName)) {
                if (linkName === pageName) {
                    return this.originalLinks[linkName];
               }
            }
        }
    }
}
