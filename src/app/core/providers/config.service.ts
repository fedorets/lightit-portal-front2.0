export class ConfigService {
    public static imgPath: string = './assets/img/';
    // public static basePath: string = `${ENV.PROTOCOL}//${ENV.API_URL}/api/`;
    // public static socketPath: string = ENV.WEBSOCKET_URL;
    public static basePath: string = 'https://portal-dev.light-it.loc/';
    public static basePathApi: string = 'https://portal-dev.light-it.loc/api/';
    public static socketPath: string = 'wss://portal-dev.light-it.loc/ws?token=';
    public static imageDefaultPath: string = 'https://pinguem.ru/static/img/avatar.c2a08a3fc438.png';
    public static googleMapKey = 'AIzaSyBmhVhypTwikmmtoKVBpuyviUzNQfYXn0k';
    public static googleSearchPath = 'https://maps.googleapis.com/maps/api/place/textsearch/json?';
    public static loginPath: string = `${ConfigService.basePathApi}auth/login/`;
    public static birthWeekPath: string = `${ConfigService.basePathApi}week_birthdays/`;
    public static userMessagesPath: string = `${ConfigService.basePathApi}user_messages/`;
    public static usersPath: string = `${ConfigService.basePathApi}users/`;
    public static messagesPath: string = `${ConfigService.basePathApi}messages/`;
    public static logoutPath: string = `${ConfigService.basePathApi}auth/logout/`;
    public static votePath: string = `${ConfigService.basePathApi}vote/`;
    public static trustLetterPath: string = `${ConfigService.basePathApi}trust_box/`;
    public static galleryPath: string = `${ConfigService.basePathApi}albums/`;
    public static vacationStatisticPath: string = `${ConfigService.basePathApi}vacations/statistic/`;
    public static vacationRequestPath: string = `${ConfigService.basePathApi}vacation_requests/`;
    public static verifyVacationRequestPath: string = `${ConfigService.basePathApi}vacation_requests/verify/`;
    public static userPath: string = `${ConfigService.basePathApi}users/`;
    public static changePassPath: string = `${ConfigService.basePathApi}auth/password/change/`;
    public static userSearch: string = `${ConfigService.basePathApi}users-search/`;
    public static commentsPath: string = `${ConfigService.basePathApi}comments/`;
    public static mentrorshipWPath: string = `${ConfigService.basePathApi}mentoring/want_be_mentor/`;
    public static mentorshipNPath: string = `${ConfigService.basePathApi}mentoring/need_mentor/`;
    public static addDevicePath: string = `${ConfigService.basePathApi}add_device/`;
    public static resetPasswordPath: string = `${ConfigService.basePathApi}auth/password/reset/`;
    public static confirmResetPasswordPath: string = `${ConfigService.basePathApi}auth/password/reset/confirm/`;

    public static defaultImageUser: string = 'assets/img/cam.png';
    public static defaultImageCompany: string = 'assets/img/placeholder-company.png';

    public static editUser: string[] = ['first_name', 'last_name', 'email', 'slack', 'phone_number', 'birth_date', 'specialization'];
    // public static editStuff: string[] = ["first_name", "last_name", "email", "skype", "phone_number", "room", "seat", "birth_date", "specialization", "email_notification", "mobile_notification", "is_staff", "is_active", "is_approved", "is_dismissed", "employment_date", "dismissed_date", "is_verified_email", "permission_groups",];
    // public static editSuperUser: string[] = [ "first_name", "last_name", "email", "skype", "phone_number", "room", "seat", "birth_date", "specialization", "email_notification", "mobile_notification", "is_superuser", "is_staff", "is_active", "is_approved", "is_dismissed", "employment_date", "dismissed_date", "is_verified_email", "permission_groups",];

    // public static googlePrjNumber: string = ENV.GOOGLE_PRJ_NUMBER;
    // public static appId: string = ENV.APP_ID;
}
