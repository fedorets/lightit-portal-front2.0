import { Injectable } from '@angular/core';

@Injectable()
export class SessionService {

    /**
     * Used for retrieve info about the user from localStorage
     * @returns object User or null
     */
    public get user() {
        return JSON.parse(localStorage.getItem('ltp.currentUser')).user;
    }

    /**
     * Used to record info about user in the localStorage
     * @param data - object User
     */
    public set user(data) {
        localStorage.setItem('ltp.currentUser', JSON.stringify(data));
    }

    /**
     * Get token from LS, used in guards, for checking if the user is logged in
     * @returns token
     */
    public get token() {
        let data = JSON.parse(localStorage.getItem('ltp.currentUser'));
        return data !== null ? data.key : null;
    }

    public set permissions(data) {
        if (data !== null) {
            let permission = {};
            data.forEach((el) => {
                permission[el.name] = el.permissions;
            });
            localStorage.setItem('permissions', JSON.stringify(permission));
        } else {
            localStorage.setItem('permissions', JSON.stringify(data));
        }
    }

    public get permissions() {
        return  JSON.parse(localStorage.getItem('permissions'));
    }

}
