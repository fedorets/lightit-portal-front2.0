export class PermissionService {

    public static PERMISSION_GROUPS = {
        VACATION_REVIEWERS: 'vacation_reviewers',
        MODERATORS: 'moderators',
        MENTOR_TABLE_EDIT: 'mentor_table_edit',
        TEAMLEAD: 'teamlead'
    };
}
