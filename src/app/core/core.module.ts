import { NgModule } from '@angular/core';

import { AuthService } from './providers/auth.service';
import { SessionService } from './providers/session.service';
import { AuthGuard } from './guards/auth.guard';
import { EnvGuard } from './guards/env.guard';
import { PermissionGuard } from './guards/permissions.guard';
import { EnvironmentService } from './providers/environment.service';
import { ProfileService } from './providers/profile.service';
import { ServerSocketService } from './providers/socket.service';

@NgModule ({
    providers: [
        AuthService,
        SessionService,
        AuthGuard,
        EnvGuard,
        PermissionGuard,
        EnvironmentService,
        ProfileService,
        ServerSocketService
    ]
})

export class CoreModule {}
