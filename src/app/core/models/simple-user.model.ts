import { ConfigService } from '../providers/config.service';

export class SimpleUser {
  public id: number;
  public first_name: string;
  public last_name: string;
  public photo: string;
  public photo_thumbnail: string;

  constructor(data) {
    this.id = data.id;
    this.first_name = data.first_name;
    this.last_name = data.last_name;
    this.photo = (data.photo === null) ? ConfigService.imageDefaultPath : data.photo;
    this.photo_thumbnail = (data.photo_thumbnail === null) ? ConfigService.imageDefaultPath : data.photo_thumbnail;
  }

  public get fullName() {
    return `${this.first_name} ${this.last_name}`;
  }
}
