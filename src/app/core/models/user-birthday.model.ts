import moment from 'moment';
moment.locale('ru');

import { ConfigService } from '../providers/config.service';

export class UserBirthday {
    public id: number;
    public first_name: string;
    public last_name: string;
    public photo_thumbnail: string;
    public birth_date: string;

    constructor(data) {
        this.id = data.id;
        this.first_name = data.first_name;
        this.last_name = data.last_name;
        this.birth_date = data.birth_date;
        this.photo_thumbnail = (data.photo_thumbnail === null) ? ConfigService.imageDefaultPath : data.photo_thumbnail;
    }

    public get fullName() {
        return `${this.first_name} ${this.last_name}`;
    }

    public get isBirthToday() {
        let today = moment().format('D MMMM');
        return this.birthDateDMMMM === today;
    }

    public get birthDateDMMMM() {
        return moment(this.birth_date).format('D MMMM');
    }
}
