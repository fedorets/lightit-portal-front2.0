import moment from 'moment';
moment.locale('ru');

import { SimpleUser } from './simple-user.model';

export class User extends SimpleUser {
  public birth_date: string;
  public skills_know: any[];
  public specialization: string;
  public skills_inform: any[];
  public hobbies: any[];
  public is_approved: boolean;
  public mentor_status: string;
  public protege_status: string;
  public mentor: any;
  public proteges: any[];
  public worked_time: any;
  public email: string;
  public slack: string;
  public skype: string;
  public room: number;
  public seat: number;
  public email_notification: boolean;
  public mobile_notification: boolean;
  public is_superuser: boolean;
  public is_staff: boolean;
  public is_active: boolean;
  public is_dismissed: boolean;
  public employment_date: string;
  public dismissed_date: string;
  public custom_hobbies: any[];
  public is_verified_email: boolean;
  public permission_groups: any[];
  public has_available_idp: boolean;
  private _phone_number: string;

  constructor(data) {
    super(data);
    this.birth_date = data.birth_date;
    this.skills_know = data.skills_know;
    this.specialization = data.specialization;
    this.skills_inform = data.skills_inform;
    this.hobbies = data.hobbies;
    this.is_approved = data.is_approved;
    this.mentor_status = data.mentor_status;
    this.protege_status = data.protege_status;
    this.mentor = data.mentor ? new SimpleUser(data.mentor) : null;
    this.proteges = data.proteges ? data.proteges.map((item) => new SimpleUser(item)) : null;
    this.worked_time = data.worked_time;
    this.email = data.email;
    this.slack = data.slack;
    this.skype = data.skype;
    this._phone_number = data.phone_number;
    this.room = data.room;
    this.seat = data.seat;
    this.email_notification = data.notification;
    this.mobile_notification = data.mobile_notification;
    this.is_superuser = data.is_superuser;
    this.is_staff = data.is_staff;
    this.is_active = data.is_active;
    this.is_dismissed = data.is_dismissed;
    this.employment_date = data.employment_date;
    this.dismissed_date = data.dismissed_date;
    this.custom_hobbies = data.custom_hobbies;
    this.is_verified_email = data.is_verified_email;
    this.permission_groups = data.permission_groups;
    this.has_available_idp = data.has_available_idp;
  }

  public get isBirthToday() {
    let today = moment().format('D MMMM');
    return this.birthDateDMMMM === today;
  }

  public get birthDateDMMMM() {
    return moment(this.birth_date).format('D MMMM');
  }

  public get withLightIt() {
    let date = moment().subtract({ days: this.worked_time.days, months: this.worked_time.months, years: this.worked_time.years });
    return  moment(date).fromNow(true);
  }

  public get birthDateDMMMMYYYY() {
    return moment(this.birth_date).format('DD MMMM YYYY');
  }

  public set phone_numb(numb) {
    this._phone_number = numb.replace(/\D+/g, '');
  }

  public get phone_numb() {
    return this._phone_number;
  }

  public get maskPhoneNumber() {
    return this.transformFormToMask(this._phone_number);
  }

  private transformFormToMask(str) {
    return `(${ str.substr(0, 3) }) ${ str.substr(3, 3) }-${ str.substring(6) }`;
  }
}
