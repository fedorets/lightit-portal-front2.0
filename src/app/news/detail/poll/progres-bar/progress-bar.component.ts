
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MzModalService } from 'ng2-materialize';

import { VotingService } from '../shared/voting.service';
import { UtilService } from '../../../../shared/providers/util.service';
import { PopupComponent } from '../popup/popup.component';
import { News } from '../../../shared/models/news.model';
import { NewsService } from '../../../shared/providers/news.service';

@Component({
    selector: 'lt-progress-bar',
    templateUrl: './progress-bar.component.html',
    styleUrls: ['./progress-bar.component.scss']
})

export class ProgressBarComponent {
    @Input() public news: News;
    @Input() public TYPES: any;
    @Output() public showPopupVoted: EventEmitter<any>;

    constructor(private votingService: VotingService,
                private utilService: UtilService,
                private newsService: NewsService,
                private modalService: MzModalService) {
        this.showPopupVoted = new EventEmitter();
    }

    public canDisplayResults() {
        return this.votingService.canDisplayResults(this.news.message);
    }

    /**
     * can vote only if you did not yet.
     */
    public checkVotePosibility(option) {
        if (!(this.news.message.content_object.poll_type === this.TYPES.CLOSED && this.news.my_vote)) {
            if (this.news.my_vote !== option.value) {
                this.getVote(option);
            }
        }
    }

    public openVotePeopleModal(option) {
            let modalTitle = `За ${ option.value } проголосовало: {%count%}`;
            this.modalService.open(PopupComponent, {
                dataSource: this.votingService.getPollResults(this.news.message.id, option.id),
                title: modalTitle
            });

            // tslint:disable
            // example output from modal
            // const modalRef = this.modalService.open(PopupComponent, { users: usersList, title: modalTitle });
            // modalRef.instance.modalComponent.onClose.subscribe((res) => {});
    }

    // TODO: rewrite with Rxjs
    private getVote(option) {
        if (this.news.message.content_object.isPollOpen) {
            let isNeedMarkAsRead = (!this.news.is_readed && (this.news.message.message_type === 'event' || this.news.message.message_type === 'poll'));
            this.votingService.vote(option.id).subscribe((message: any) => {
                message.forEach((item, index) => {
                    this.news.message.options[index].votes = item.votes;
                    this.news.my_vote = option.value;

                    if (isNeedMarkAsRead) {
                        this.newsService.markAsReaded(this.news.id).subscribe((res) => {
                            this.news.is_readed = res;
                        }, (err) => console.log('Новость не была отмечена как прочитанная.'));
                    }
                });
                this.utilService.getMaterializeToast('Спасибо, голос учтен.');
            }, () => this.utilService.getMaterializeToast('Произошла ошибка.', 'failure'));
        } else {
            this.utilService.getMaterializeToast('Голосование закрыто.', 'failure');
        }
    }
}
