import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ProfileService } from '../../../../core/providers/profile.service';
import { User } from '../../../../core/models/user.model';
import { ConfigService } from '../../../../core/providers/config.service';
import { Vote } from '../../../shared/models/vote.model';
import { SimpleUser } from '../../../../core/models/simple-user.model';

@Injectable()
export class VotingService {
    public user: User;
    public POLL_TYPES = {
        PUBLIC: 'public',
        PRIVATE: 'private',
        CLOSED: 'closed'
    };

    constructor(private profileService: ProfileService,
                private http: HttpClient) {
        this.user = this.profileService.user;
    }

    // TODO: change is_superuser to new permission group
    public canDisplayResults(message) {
        if (message.content_object.poll_type === this.POLL_TYPES.PUBLIC) {
            return true;
        } else if (this.user.id === message.author.id) {
            return true;
        } else if (this.user.is_superuser) {
            return true;
        } else {
            return (message.message_type === 'event');
        }
    }

    // TODO: change is_staff to new permission group
    public canDisplayNotAnswered(message) {
        if (this.user.is_staff) {
            if (message.message_type === 'event') {
                return true;
            } else {
                return (message.message_type === 'poll' && message.content_object.poll_type === this.POLL_TYPES.PUBLIC);
            }
        } else {
            return false;
        }
    }

    /**
     * @returns array of SimpleUser
     */
    public getPollResults(messageId, optionId) {
        return this.http.get(`${ConfigService.messagesPath + messageId}/poll_results/`)
            .map((res: any) => {
                let votes = res.options.map((option) => new Vote(option));
                let pollResult = votes.find((vote) => vote.id === optionId);
                return pollResult.detail_votes.map((item) => item.user);
            });
    }

    /**
     * called when user get vote
     * @param id of variant answer
     * @returns array of options with new poll results
     */
    public vote(id) {
        return this.http.post(ConfigService.votePath, {option: id})
            .map((data: any) => {
                let tmp = [];
                data.options.forEach((i) => {
                    tmp.push({
                        id: i.id,
                        votes: i.votes
                    });
                });
                return tmp;
            });
    }

    /**
     * @param id of message
     * @returns array of users, who did not vote
     */
    public getWhoNotVoted(id) {
        return this.http.get(`${ConfigService.messagesPath}${id}/not_voted_users/`)
            .map((res: any[]) => res.map((user) => new SimpleUser(user)));
    }
}
