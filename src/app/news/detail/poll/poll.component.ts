
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MzModalService } from 'ng2-materialize';
import { Subscription } from 'rxjs/Subscription';

import { News } from '../../shared/models/news.model';
import { User } from '../../../core/models/user.model';
import { VotingService } from './shared/voting.service';
import { PopupComponent } from './popup/popup.component';
import { ServerSocketService } from '../../../core/providers/socket.service';

@Component({
    selector: 'lt-poll',
    templateUrl: './poll.component.html',
    styleUrls: ['./poll.component.scss']
})

export class PollComponent implements OnInit, OnDestroy {
    public user: User;
    public TYPES: any;
    public socket: any;
    public socketSubscription: Subscription;
    @Input() public news: News;

    constructor(private votingService: VotingService,
                private modalService: MzModalService,
                private socketService: ServerSocketService) {
        this.TYPES = this.votingService.POLL_TYPES;

        this.socket = this.socketService.connect();
        this.socketSubscription = this.socket.subscribe((dataChanges: any) => {
            console.log('received message from server: ', dataChanges);

            switch (dataChanges.event) {
                case 'update':
                    if (dataChanges.data.options) {
                        dataChanges.data.options.forEach((option, index) => {
                            this.news.message.options[index].votes = option.votes;
                        });
                    }
                    break;
                default: break;
            }
        });
    }

    public getWhoNotVote(id) {
        let modalTitle = 'Не проголосовало: {%count%}';
        this.modalService.open(PopupComponent, {
            dataSource: this.votingService.getWhoNotVoted(id),
            title: modalTitle
        });
    }

    public canDisplayNotAnswered() {
        return this.votingService.canDisplayNotAnswered(this.news.message);
    }

    public ngOnInit() {
        this.socketService.send('join', 'votes', this.news.message.id);

    }

    public ngOnDestroy() {
        this.socketService.send('leave', 'votes', this.news.message.id);
        this.socketSubscription.unsubscribe();
    }
}
