
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MzBaseModal, MzModalComponent } from 'ng2-materialize';

import { SimpleUser } from '../../../../core/models/simple-user.model';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'lt-popup',
    templateUrl: './popup.component.html',
    styleUrls: ['./popup.component.scss']
})

export class PopupComponent extends MzBaseModal implements OnInit {
    @Input() public dataSource: Observable<any>;
    @Input() public title: string;
    @ViewChild('modal') public modal: MzModalComponent;
    public users: SimpleUser[];

    constructor() {
        super();
    }

    // example output
    // public closeModal(value) {
    //     this.modal.onClose.emit(value);
    //     this.modal.close();
    // }
    public ngOnInit() {
        this.dataSource.subscribe((users: SimpleUser[]) => {
            this.users = users;
            this.title = this.title.replace(/{%count%}/, users.length.toString());
        });
    }
}
