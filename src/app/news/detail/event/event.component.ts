
import { Component, Input } from '@angular/core';

@Component({
    selector: 'lt-event',
    templateUrl: './event.component.html',
    styleUrls: ['./event.component.scss']
})

export class EventComponent {
    @Input() public eventData: any;
}
