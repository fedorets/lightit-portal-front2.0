
import { Component, Input } from '@angular/core';

import { News } from '../shared/models/news.model';

@Component({
    selector: 'lt-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})

export class DetailComponent {
    @Input() public news: News;
    @Input() public isVisibleComments: boolean;

    constructor() {}
}
