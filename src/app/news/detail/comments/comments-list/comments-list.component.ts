import { Component, EventEmitter, Input, Output } from '@angular/core';

import { Comment } from '../shared/comment.model';

@Component({
    selector: 'lt-comments-list',
    templateUrl: './comments-list.component.html',
    styleUrls: ['./comments-list.component.scss']
})

export class CommentsListComponent {
    @Input() public comments: Comment[];
    @Input() public messageId: number;
    @Output() public deleteCommentEvent: EventEmitter<number>;

    public isVisibleAllList: boolean;

    constructor() {
        this.isVisibleAllList = false;
        this.deleteCommentEvent = new EventEmitter();
    }

    public deleteCommentSendEvent(id) {
        this.deleteCommentEvent.emit(id);
    }
    
    public checkStateList(index) {
        return !this.isVisibleAllList ? index > this.comments.length - 6 : true;
    }
}
