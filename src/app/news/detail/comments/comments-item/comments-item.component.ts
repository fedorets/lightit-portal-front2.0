import { Component, EventEmitter, Input, Output } from '@angular/core';

import { Comment } from '../../../shared/models/comment.model';
import { CommentsService } from '../shared/comments.service';
import { User } from '../../../../core/models/user.model';
import { ProfileService } from '../../../../core/providers/profile.service';
import { UtilService } from '../../../../shared/providers/util.service';

@Component({
    selector: 'lt-comments-item',
    templateUrl: './comments-item.component.html',
    styleUrls: ['./comments-item.component.scss']
})

export class CommentsItemComponent {
    @Input() public comment: Comment;
    @Input() public messageId: number;
    @Output() public deleteCommentEvent: EventEmitter<number> = new EventEmitter();
    public user: User;

    constructor(private commentsService: CommentsService,
                private profileService: ProfileService,
                private utilService: UtilService) {
        this.user = this.profileService.user;
    }

    public removeComment(id) {
        this.commentsService.removeComment(this.messageId, id).subscribe(() => {
            this.deleteCommentEvent.emit(id);
            this.utilService.getMaterializeToast('Комментарий успешно удален.');
        }, (err) => console.log('Не удалось удалить комментарий'));
    }
}
