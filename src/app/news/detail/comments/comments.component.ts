import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { CommentsService } from './shared/comments.service';
import { ProfileService } from '../../../core/providers/profile.service';
import { User } from '../../../core/models/user.model';
import { Comment } from './shared/comment.model';
import { ServerSocketService } from '../../../core/providers/socket.service';

@Component({
    selector: 'lt-comments',
    templateUrl: './comments.component.html',
    styleUrls: ['./comments.component.scss']
})

export class CommentsComponent implements OnInit, OnDestroy {
    public comments: Comment[];
    public user: User;
    public socket: any;
    public socketSubscription: Subscription;
    @Input() public messageId: number;

    constructor(private commentsService: CommentsService,
                private profileService: ProfileService,
                private socketService: ServerSocketService) {
        this.user = this.profileService.user;

        this.socket = this.socketService.connect();
        this.socketSubscription = this.socket.subscribe((dataChanges) => {
            console.log(dataChanges);
            switch (dataChanges.event) {
                case 'create' :
                    if (dataChanges.type === 'comment') {
                        if (!this.commentsService.isCurrentCommentExist(dataChanges.data.id, this.comments)) {
                            this.comments.push(new Comment(dataChanges.data));
                        }
                    }
                    break;

                case 'delete' :
                    if (this.commentsService.isCurrentCommentExist(dataChanges.data, this.comments)) {
                        this.comments = this.comments.filter((item) => item.id !== dataChanges.data);
                    }
                    break;
                default:
                break;
            }
        });
    }

    public getComments() {
        this.commentsService.getComments(this.messageId).subscribe((res: any) => {
            this.comments = res;
        }, (err) => {
            console.log('Ошибка получения комментов', err);
        });
    }

    /**
     * emit of addCommentEvent from comments-form
     * @param comment - new Comment()
     */
    public addCommentToList(comment: Comment) {
        if (!this.commentsService.isCurrentCommentExist(comment.id, this.comments)) {
            this.comments.push(comment);
        }
    }

    /**
     * emit of deleteCommentEvent from comments-list
     * @param id of deleted comment
     */
    public deleteCommentFromList(id) {
        if (this.commentsService.isCurrentCommentExist(id, this.comments)) {
            this.comments = this.comments.filter((item) => item.id !== id);
        }
    }

    public ngOnInit() {
        this.getComments();
        this.socketService.send('join', 'comments', this.messageId);
    }

    public ngOnDestroy() {
        this.socketService.send('leave', 'comments', this.messageId);
        this.socketSubscription.unsubscribe();
    }
}
