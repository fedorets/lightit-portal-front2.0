import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { CommentsService } from '../shared/comments.service';
import { UtilService } from '../../../../shared/providers/util.service';
import { Comment } from '../shared/comment.model';
import { ErrorService } from '../../../../shared/providers/errors.service';

@Component({
    selector: 'lt-comments-form',
    templateUrl: './comments-form.component.html',
    styleUrls: ['./comments-form.component.scss']
})

export class CommentsFormComponent {
    public commentForm: FormGroup;
    public errorMessages: any;
    @Input() public messageId: number;
    @Output() public addCommentEvent: EventEmitter<Comment>;

    constructor(private fb: FormBuilder,
                private commentsService: CommentsService,
                private utilService: UtilService,
                private errorService: ErrorService) {
        this.addCommentEvent = new EventEmitter();
        this.errorMessages = this.errorService.errorMessages;

        this.commentForm = this.fb.group({
            text: [null, [
                Validators.minLength(1),
                Validators.maxLength(500),
                Validators.required
            ]],
            message: [null]
        });
    }

    public submit($event, form) {
        $event.preventDefault();
        this.commentForm.patchValue({message: this.messageId});
        if (form.valid) {
            this.commentsService.addComment(form.value).subscribe((data: Comment) => {
                this.addCommentEvent.emit(data);
                this.commentForm.reset();
            }, (err) => this.utilService.getMaterializeToast('Ошибка добавления.', 'failure'));
        }
    }
}
