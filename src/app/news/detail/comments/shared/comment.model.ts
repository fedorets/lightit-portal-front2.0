
import moment from 'moment';
moment.locale('ru');

import { SimpleUser } from '../../../../core/models/simple-user.model';

export class Comment {
    public id: number;
    public create_dt: string;
    public edit_dt: string;
    public message: number;
    public text: string;
    public user: SimpleUser;
    constructor(data) {
        this.id = data.id;
        this.message = data.message;
        this.text = data.text;
        this.user = (data.user) ? new SimpleUser(data.user) : null;
        this.create_dt = moment(data.create_dt).format('DD MMM YYYY HH:mm ');
        this.edit_dt = moment(data.edit_dt).format('DD MMM YYYY HH:mm');
    }
}
