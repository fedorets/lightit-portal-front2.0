import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ConfigService } from '../../../../core/providers/config.service';
import { Comment } from './comment.model';

@Injectable()
export class CommentsService {
    constructor(private http: HttpClient) {}

    /**
     * after submit comment form
     * @param data - message id and message text
     * @returns Comment object
     */
    public addComment(data) {
        return this.http.post(ConfigService.commentsPath, data).map((res) => new Comment(res));
    }

    public removeComment(msgId, commentId) {
        return this.http.delete(`${ConfigService.messagesPath}${msgId}/comments/${commentId}/`);
    }

    public getComments(id) {
        return this.http.get(`${ConfigService.messagesPath}${id}/comments/`).map((res: any[]) => {
            return res.map((comment) => new Comment(comment));
        });
    }

    public isCurrentUserAuthor(commentId, comments, userId) {
        let currComments = comments.find((item) => item.message.author.id === userId);
        return !!currComments;
    }

    public isCurrentCommentExist(commentId, comments) {
        return !!(comments.find((item) => item.id === commentId));
    }

}
