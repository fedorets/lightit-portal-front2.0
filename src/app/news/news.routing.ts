import { Routes } from '@angular/router';

import { NewsComponent } from './news.component';
import { NewsResolver } from './shared/providers/news.resolver';

export const newsRoutes: Routes = [
    { path: '', component: NewsComponent, pathMatch: 'full', resolve: { news: NewsResolver } },
    { path: 'news', component: NewsComponent, resolve: { news: NewsResolver } }
];
