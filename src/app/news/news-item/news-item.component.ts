import { Component, EventEmitter, Input, Output } from '@angular/core';

import { News } from '../shared/models/news.model';
import { NewsService } from '../shared/providers/news.service';
import { ProfileService } from '../../core/providers/profile.service';
import { UtilService } from '../../shared/providers/util.service';
import { User } from '../../core/models/user.model';

@Component({
    selector: 'lt-news-item',
    styleUrls: ['./news-item.component.scss'],
    templateUrl: './news-item.component.html'
})

export class NewsItemComponent {
    @Input() public singleNews: News;
    @Output() public toggleEventEmitter: EventEmitter<number>;
    @Output() public deleteNews: EventEmitter<number>;
    @Input() public isNewsDetailOpen: boolean;
    public isVisibleComments: boolean;
    public user: User;
    public TYPES_NEWS;

    constructor(private newsService: NewsService,
                private profileService: ProfileService,
                private utilService: UtilService) {
        this.toggleEventEmitter = new EventEmitter();
        this.deleteNews = new EventEmitter();
        this.user = this.profileService.user;
        this.isVisibleComments = false;
        this.TYPES_NEWS = this.newsService.TYPES_NEWS;
    }

    public toggleEvent(id) {
        if (this.isNewsDetailOpen) {
            this.isNewsDetailOpen = false;
            this.toggleEventEmitter.emit(null);
        } else {
            this.toggleEventEmitter.emit(id);
        }
        this.isVisibleComments = false;
        this.markAsReaded(id);

    }

    public toggleComments(id) {
        if (!(!this.singleNews.is_readed && !this.isNewsDetailOpen)) {
            this.isVisibleComments = !this.isVisibleComments;
            this.isNewsDetailOpen = true;
            this.toggleEventEmitter.emit(id);
            this.markAsReaded(id);
        }
    }

    public receiveChoice() {
        this.newsService.deleteNews(this.singleNews.message.id).subscribe(() => {
            this.singleNews.was_removed = true;
        }, (err) => this.utilService.getMaterializeToast('Ошибка. Попробуйте еще раз через некоторое время.'));
    }

    public changeFavorite(val, id) {
        this.newsService.markAsFavorite(val, id).subscribe((res: News) => {
            this.singleNews.is_favorite = res.is_favorite;
            if (this.singleNews.is_favorite) {
                this.utilService.getMaterializeToast('Добавлено в избранное.');
            } else {
                this.utilService.getMaterializeToast('Удалено из избранного.', 'inform');
            }
        }, () => {
            this.utilService.getMaterializeToast('Ошибка. Попробуйте еще раз через некоторое время.', 'failure');
        });
    }

    public removeNews() {
        this.deleteNews.emit(this.singleNews.message.id);
    }

    private markAsReaded(id) {
        if (!this.singleNews.is_readed && this.singleNews.message.message_type === this.TYPES_NEWS.ADVERT) {
            this.newsService.markAsReaded(id).subscribe((res) => {
                this.singleNews.is_readed = res;
            }, (err) => console.log('Ошибка. Новость не была отмечена как прочитанная.'));
        }
    }

}
