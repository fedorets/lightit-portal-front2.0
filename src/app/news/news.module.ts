import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NewsComponent } from './news.component';
import { NewsService } from './shared/providers/news.service';
import { newsRoutes } from './news.routing';
import { SharedModule } from '../shared/shared.module';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { NewsListComponent } from './news-list/news-list.component';
import { NewsResolver } from './shared/providers/news.resolver';
import { NewsItemComponent } from './news-item/news-item.component';
import { DetailComponent } from './detail/detail.component';
import { RemoveNewsBtnComponent } from './shared/components/remove-news-btn/remove-news-btn.component';
import { EventComponent } from './detail/event/event.component';
import { PollComponent } from './detail/poll/poll.component';
import { VotingService } from './detail/poll/shared/voting.service';
import { ProgressBarComponent } from './detail/poll/progres-bar/progress-bar.component';
import { PopupComponent } from './detail/poll/popup/popup.component';
import { CommentsFormComponent } from './detail/comments/comments-form/comments-form.component';
import { CommentsService } from './detail/comments/shared/comments.service';
import { CommentsComponent } from './detail/comments/comments.component';
import { CommentsListComponent } from './detail/comments/comments-list/comments-list.component';
import { CommentsItemComponent } from './detail/comments/comments-item/comments-item.component';
import { NewMsgCountComponent } from './shared/components/new-msg-count/new-msg-count.component';
import { AddPostModule } from '../add-post/add-post.module';

@NgModule ({
    declarations: [
        NewsComponent,
        SearchBarComponent,
        NewsListComponent,
        NewsItemComponent,
        DetailComponent,
        RemoveNewsBtnComponent,
        ProgressBarComponent,
        PopupComponent,
        EventComponent,
        PollComponent,
        CommentsComponent,
        CommentsFormComponent,
        CommentsListComponent,
        CommentsItemComponent,
        NewMsgCountComponent
    ],
    providers: [
        NewsService,
        NewsResolver,
        VotingService,
        CommentsService
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(newsRoutes),
        SharedModule,
        AddPostModule,
        ReactiveFormsModule,
        FormsModule
    ],
    entryComponents: [
        PopupComponent
    ]
})

export class NewsModule {}
