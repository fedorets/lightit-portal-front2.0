import { Component, Input } from '@angular/core';

import { News } from '../shared/models/news.model';

@Component({
    selector: 'lt-news-list',
    styleUrls: ['./news-list.component.scss'],
    templateUrl: './news-list.component.html'
})

export class NewsListComponent {
    @Input() public news: News[];
    public currentActiveNews: number;

    /**
     * it is necessary for show only one news at one time
     * @param id news with opened detail
     */
    public toggleEventEmitter(id) {
        this.currentActiveNews = id;
    }

    /**
     * emit by deleteNews from news-item
     * @param id of deleted news
     */
    public filterNews(id) {
        this.news = this.news.filter((item) => item.message.id !== id);
    }
}
