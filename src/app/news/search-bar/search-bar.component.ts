import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
    selector: 'lt-search-bar',
    styleUrls: ['./search-bar.component.scss'],
    templateUrl: './search-bar.component.html'
})

export class SearchBarComponent {
    public filterForm: FormGroup;
    @Output() public onChangeSearchFilter: EventEmitter<any>;

    constructor(private fb: FormBuilder) {
        this.onChangeSearchFilter = new EventEmitter();

        this.filterForm = fb.group({
            search: [null],
            favorite: [false],
            event: [false],
            poll: [false],
            advert: [false]
        });

        this.filterForm.valueChanges.debounceTime(400).subscribe((val) => {
            this.onChangeSearchFilter.emit(val);
        });
    }

    public clearSearch() {
        this.filterForm.patchValue({search: null});
    }
}
