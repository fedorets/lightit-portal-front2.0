import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { NewsService } from './shared/providers/news.service';
import { UserMessages } from './shared/models/user-messages.model';
import { ServerSocketService } from '../core/providers/socket.service';
import { News } from './shared/models/news.model';

@Component({
    selector: 'lt-news',
    templateUrl: './news.component.html',
    styleUrls: ['news.component.scss']
})

export class NewsComponent implements OnInit, OnDestroy {
    public news: News[];
    public filters: any;
    public isVisibleLoader: boolean;
    public socket: any;
    public socketSubscription: Subscription;
    public newMessagesCount: number = 0;

    constructor(private route: ActivatedRoute,
                private newsService: NewsService,
                private socketService: ServerSocketService) {
        this.socket = this.socketService.connect();
        this.socketSubscription = this.socket.subscribe((dataChanges) => {
            console.log(dataChanges);

            switch (dataChanges.event) {
                case 'delete':
                    if (!this.newsService.isCurrentNewsExist(dataChanges.data, this.news)) {
                        this.news.find((item) => item.message.id === dataChanges.data)
                            .was_removed = true;
                    }
                    break;
                case 'update':
                    if (dataChanges.type === 'comment_count') {
                        this.news.find((item) => item.message.id === dataChanges.data.message)
                            .message.comments_count = dataChanges.data.comment_count;
                    }
                    break;
                case 'create':
                    if (dataChanges.type === 'event') {
                        if (!this.newsService.isCurrentUserAuthor(dataChanges.data.author.id)) {
                            this.newMessagesCount += 1;
                        }
                    }
                    break;
                default:
                    break;
            }
        });

        this.filters = {};

        // first news come from news.resolver
        this.news = this.route.snapshot.data['news'].results;
        this.filters.page = this.route.snapshot.data['news'].next ? 2 : null;
    }

    /**
     * Output from search-bar after change one of filters
     * @param filters - object with filters fields
     */
    public changeFilters(filters) {
        this.filters = filters;
        this.filters.page = 1;
        this.getNews();
    }

    public getNews(nextPage = false) {
        this.newMessagesCount = 0;
        this.isVisibleLoader = true;
        this.newsService.getNews(this.filters).subscribe((userMessages: UserMessages) => {
            this.news = nextPage ? [...this.news, ...userMessages.results] : userMessages.results;
            this.filters.page = userMessages.next ? ++this.filters.page : null;
            this.isVisibleLoader = false;
        }, (err) => console.log('Ошибка при получении новостей'));
    }

    public updateNewsList() {
        this.filters = {};
        this.getNews();
    }

    public ngOnInit() {
        this.socketService.send('join', 'messages', '');
    }

    public ngOnDestroy() {
        this.socketService.send('leave', 'messages', '');
        this.socketSubscription.unsubscribe();
    }
}
