import { Message } from './message.model';

export class News {
  public id: number;
  public user: number;
  public message: Message;
  public my_vote: string;
  public is_favorite: boolean;
  public is_readed: boolean;
  public read_dt: boolean;
  public was_removed: boolean;

  constructor(data) {
    this.id = data.id;
    this.user = data.user;
    this.message = new Message(data.message);
    this.my_vote = data.my_vote;
    this.is_favorite = data.is_favorite;
    this.is_readed = data.is_readed;
    this.read_dt = data.read_dt;
    this.was_removed = false;
  }
}
