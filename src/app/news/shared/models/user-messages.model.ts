import { News } from './news.model';

export class UserMessages {
    public count: number;
    public previous: string;
    public next: string;
    public results: News[];

    constructor(data) {
        this.count = data.count;
        this.previous = data.previous;
        this.next = data.next;
        this.results = data.results.map((news) => new News(news));
    }
}
