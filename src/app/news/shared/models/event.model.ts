import moment from 'moment';
moment.locale('ru');

export class Event {
  public id: number;
  public date_time: string;
  public location: string;
  public location_url: string;
  public poll_end_date: string;
  public icon: string;

  constructor(data) {
    this.id = data.id;
    this.date_time = moment(data.date_time).format('D MMM YYYY h:mm');
    this.location = data.location;
    this.location_url = data.location_url;
    this.poll_end_date = data.poll_end_date;
    this.icon = 'fa-users';
  }

  public get isPollOpen() {
    return moment().isSameOrBefore(this.poll_end_date);
  }

  public get formatPollEndDate() {
    return moment(this.poll_end_date).format('L');
  }
}
// date_time    :    "2017-10-06T14:10:00Z"
// id  :  27
// location  :  "Козак Палац, малый зал"
// location_url  :  "https://goo.gl/maps/H9JteotcPw"
// poll_end_date  :  "2017-10-04"
