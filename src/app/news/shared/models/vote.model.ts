import { DetailVotes } from './detail-votes.model';

export class Vote {
  public id: number;
  public value: string;
  public votes: number;
  public detail_votes: DetailVotes[];

    constructor(data) {
    this.id = data.id;
    this.value = data.value;
    this.votes = (data.votes) ? data.votes : 0;
    if (data.detail_votes) {
      this.detail_votes = [];
      data.detail_votes.forEach((i) => {
        this.detail_votes.push(new DetailVotes(i));
      });
    }
  }

  public getPercent(options, current) {
    let sum = 0;
    options.forEach((item) => sum += item.votes);
    return (sum !== 0) ? current * (100 / sum) : 0;
  }
}
