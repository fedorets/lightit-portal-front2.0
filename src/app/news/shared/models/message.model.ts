import moment from 'moment';
moment.locale('ru');

import { Poll } from './poll.model';
import { Remind } from './remind.model';
import { Event } from './event.model';
import { Advert } from './advert.model';
import { Vote } from './vote.model';
import { SimpleUser } from '../../../core/models/simple-user.model';

export class Message {
  public author: SimpleUser;
  public comments_count: number;
  public content_object: any;
  public content_type: number;
  public create_dt: string;
  public edit_dt: string;
  public id: number;
  public is_actual: boolean;
  public message_type: string;
  public object_id: number;
  public options: any[];
  public text: string;
  public title: string;

  constructor(data) {
    this.author = (data.author !== null) ? new SimpleUser(data.author) : null;
    this.comments_count = data.comments_count;
    this.content_type = data.content_type;
    this.create_dt = moment(data.create_dt).format('D MMM YYYY h:mm');
    this.edit_dt = moment(data.edit_dt).format('D MMM YYYY h:mm ');
    this.id = data.id;
    this.is_actual = data.is_actual;
    this.message_type = data.message_type;
    this.object_id = data.object_id;
    this.options = data.options;
    this.text = data.text;
    this.title = data.title;

    if (this.options.length > 0) {
      let tmp = [];
      this.options.forEach((i) => {
        tmp.push(new Vote(i));
      });
      this.options = tmp;
    }
    switch (data.message_type) {
      case 'event':
        this.content_object = new Event(data.content_object);
        break;
      case 'poll':
        this.content_object = new Poll(data.content_object);
        break;
      case 'remind':
        this.content_object = new Remind(data.content_object);
        break;
      case 'advert':
        this.content_object = new Advert(data.content_object);
        break;
      default: break;
    }
  }
}
