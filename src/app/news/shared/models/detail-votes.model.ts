
import { SimpleUser } from '../../../core/models/simple-user.model';

export class DetailVotes {
  public option: number;
  public user: SimpleUser;
  constructor(data) {
    this.option = data.option;
    this.user = new SimpleUser(data.user);
  }
}
