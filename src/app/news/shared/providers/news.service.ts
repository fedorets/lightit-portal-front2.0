import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URLSearchParams } from '@angular/http';

import { ConfigService } from '../../../core/providers/config.service';
import { ErrorService } from '../../../shared/providers/errors.service';
import { UserMessages } from '../models/user-messages.model';
import { ProfileService } from '../../../core/providers/profile.service';
import { News } from '../models/news.model';

@Injectable()
export class NewsService {
    public TYPES_NEWS = {
        EVENT: 'event',
        POLL: 'poll',
        ADVERT: 'advert'
    };
    constructor(private http: HttpClient,
                private errorService: ErrorService,
                private profileService: ProfileService) {}

    /**
     * @returns {any}
     */
    public getNews(filters = null) {
        return this.http.get(ConfigService.userMessagesPath, { params: this.params(filters) })
            .map((res: any) => new UserMessages(res))
            .catch((err) => this.errorService.getErrors(err));
    }

    /**
     * called after first click on news
     * @param id of news
     * @returns new value of 'is_readed' property
     */
    public markAsReaded(id) {
        return this.http.patch(`${ConfigService.userMessagesPath + id}/`, {is_readed: true})
            .map((res: any) => res.is_readed);
    }

    /**
     * @param data: is_favorite
     * @param id of single news
     * @returns new news object
     */
    public markAsFavorite(data: boolean, id: number) {
        return this.http.patch(`${ConfigService.userMessagesPath + id}/`, {is_favorite: data});
    }

    /**
     * @param id of news.message
     * @returns no response
     */
    public deleteNews(id) {
        return this.http.delete(`${ConfigService.messagesPath}${id}/`);
    }

    public isCurrentUserAuthor(idAuthorMessage) {
        return idAuthorMessage === this.profileService.user.id;
    }

    public isCurrentNewsExist(newsId: number, news: News[]) {
        let result = news.find((item) => item.id === newsId);
        return !!result;
    }

    private params(options) {
        if (options) {
            let params: any = new URLSearchParams();
            for (let property in options) {
                if (options.hasOwnProperty(property)) {
                    if ((property === 'search' || property === 'page') && options[property] !== null) {
                        params.append(property, options[property]);
                    } else if (options[property]) {
                        params.append('type', property);
                    }
                }
            }
            console.log(params);
            return params;
        }
    }

}
