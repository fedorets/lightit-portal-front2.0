import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import 'rxjs/add/observable/of';

import { NewsService } from './news.service';

@Injectable()
export class NewsResolver implements Resolve<any> {
    constructor(public newsService: NewsService) {}
    public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
       return this.newsService.getNews();
    }
}
