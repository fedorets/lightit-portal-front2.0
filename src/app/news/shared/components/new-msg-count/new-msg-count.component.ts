import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 'lt-new-msg-count',
    templateUrl: './new-msg-count.component.html',
    styleUrls: ['./new-msg-count.component.scss']
})

export class NewMsgCountComponent {
    @Output() public updateNewsEmit: EventEmitter<any>;
    @Input() public messagesCount: number;

    constructor() {
        this.updateNewsEmit = new EventEmitter();
    }

    public updateNewsEvent() {
        this.updateNewsEmit.emit();
    }
}
