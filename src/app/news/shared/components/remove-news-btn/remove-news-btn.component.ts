import { Component, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'lt-remove-news-btn',
    templateUrl: './remove-news-btn.component.html',
    styleUrls: ['./remove-news-btn.component.scss']
})

export class RemoveNewsBtnComponent {
    @Output() public answer: EventEmitter<any>;
    public isConfirmVisible: boolean;

    constructor() {
        this.isConfirmVisible = false;
        this.answer = new EventEmitter();
    }

    public sendChoice(choice) {
        if (choice) {
            this.answer.emit();
        }
        this.isConfirmVisible = false;
    }

    public toggleConfirm() {
        this.isConfirmVisible = !this.isConfirmVisible;
    }
}
